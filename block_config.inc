ZBB=disable
ZBS=disable
ZBR=disable
ZBT=disable
ZBM=disable
ZBP=disable
ZBF=disable
ZBE=disable
ZBA=disable
ZBC=disable

#Bit Manipulation Instruction Extensions
#https://github.com/riscv/riscv-bitmanip/blob/master/bitmanip-draft.pdf
BMI_ALL=enable


SYNTH = SIM	
#Targeting Architecture
XLEN =32
# Directory for the targeting bsv
EG = src
#Verbosity
VERBOSITY = 1
SYNTH=SIM


