package Tb_bitmanip;
import bitmanip :: *;

`include "check.bsv"
`include "Tb_test_cases.defines"
`include "Logger.bsv"

function Action passString( Reg#(Bit#(32)) i );
return action
   case(i)
     0 : `logLevel( bitmanip,1,$format ("\t Sent ANDN to dut"))
     1 : `logLevel( bitmanip,1,$format ("\t Sent ORN to dut"))
     2 : `logLevel( bitmanip,1,$format ("\t Sent XORN to dut"))
     3 : `logLevel( bitmanip,1,$format ("\t Sent SLO to dut"))
     4 : `logLevel( bitmanip,1,$format ("\t Sent SRO to dut"))
     5 : `logLevel( bitmanip,1,$format ("\t Sent ROL to dut"))
     6 : `logLevel( bitmanip,1,$format ("\t Sent ROR to dut"))
     7 : `logLevel( bitmanip,1,$format ("\t Sent SBCLR to dut"))
     8 : `logLevel( bitmanip,1,$format ("\t Sent SBEXT to dut"))
     9 : `logLevel( bitmanip,1,$format ("\t Sent SBSET to dut"))
     10: `logLevel( bitmanip,1,$format ("\t Sent SBINV to dut"))

     11 : `logLevel( bitmanip,1,$format ("\t Sent PACKL to dut"))
     12 : `logLevel( bitmanip,1,$format ("\t Sent PACKU to dut"))
     13 : `logLevel( bitmanip,1,$format ("\t Sent SLOI to dut"))
     14 : `logLevel( bitmanip,1,$format ("\t Sent SROI to dut"))
     15 : `logLevel( bitmanip,1,$format ("\t Sent RORI to dut"))
     16 : `logLevel( bitmanip,1,$format ("\t Sent SH1ADD to dut"))
     17 : `logLevel( bitmanip,1,$format ("\t Sent SH2ADD to dut"))
     18 : `logLevel( bitmanip,1,$format ("\t Sent SH3ADD to dut"))
     19 : `logLevel( bitmanip,1,$format ("\t Sent MIN to dut"))
     20 : `logLevel( bitmanip,1,$format ("\t Sent MAX to dut"))

     21 : `logLevel( bitmanip,1,$format ("\t Sent MINU to dut"))
     22 : `logLevel( bitmanip,1,$format ("\t Sent MAXU to dut"))
     23 : `logLevel( bitmanip,1,$format ("\t Sent ADDWU to dut"))
     24 : `logLevel( bitmanip,1,$format ("\t Sent SUBWU to dut"))
     25 : `logLevel( bitmanip,1,$format ("\t Sent ADDU.W to dut"))
     26 : `logLevel( bitmanip,1,$format ("\t Sent SUBU.W to dut"))
     27 : `logLevel( bitmanip,1,$format ("\t Sent SLOW to dut"))
     28 : `logLevel( bitmanip,1,$format ("\t Sent SROW to dut"))
     29 : `logLevel( bitmanip,1,$format ("\t Sent RORW to dut"))
     30 : `logLevel( bitmanip,1,$format ("\t Sent ROLW to dut"))

     31 : `logLevel( bitmanip,1,$format ("\t Sent GREV to dut"))
     32 : `logLevel( bitmanip,1,$format ("\t Sent SBCLRI to dut"))
     33 : `logLevel( bitmanip,1,$format ("\t Sent SBINVI to dut"))
     34 : `logLevel( bitmanip,1,$format ("\t Sent SBEXTI to dut"))
     35 : `logLevel( bitmanip,1,$format ("\t Sent SBSETI to dut"))
     36 : `logLevel( bitmanip,1,$format ("\t Sent GREVI to dut"))
     37 : `logLevel( bitmanip,1,$format ("\t Sent GORC to dut"))
     38 : `logLevel( bitmanip,1,$format ("\t Sent GORCI to dut"))
     39 : `logLevel( bitmanip,1,$format ("\t Sent GORCIW to dut"))
     40 : `logLevel( bitmanip,1,$format ("\t Sent GREVW to dut"))

     41 : `logLevel( bitmanip,1,$format ("\t Sent GREVIW to dut"))
     42 : `logLevel( bitmanip,1,$format ("\t Sent GORCW to dut"))
     43 : `logLevel( bitmanip,1,$format ("\t Sent SBCLRW to dut"))
     44 : `logLevel( bitmanip,1,$format ("\t Sent SBEXTW to dut"))
     45 : `logLevel( bitmanip,1,$format ("\t Sent SBSETW to dut"))
     46 : `logLevel( bitmanip,1,$format ("\t Sent SBINVW to dut"))
     47 : `logLevel( bitmanip,1,$format ("\t Sent SBCLRIW to dut"))
     48 : `logLevel( bitmanip,1,$format ("\t Sent SBSETIW to dut"))
     49 : `logLevel( bitmanip,1,$format ("\t Sent SLOIW to dut"))
     50 : `logLevel( bitmanip,1,$format ("\t Sent SROIW to dut"))

     51 : `logLevel( bitmanip,1,$format ("\t Sent RORIW to dut"))
     52 : `logLevel( bitmanip,1,$format ("\t Sent SBINVIW to dut"))
     53 : `logLevel( bitmanip,1,$format ("\t Sent CLZ to dut"))
     54 : `logLevel( bitmanip,1,$format ("\t Sent CTZ to dut"))
     55 : `logLevel( bitmanip,1,$format ("\t Sent PCNT to dut"))
     56 : `logLevel( bitmanip,1,$format ("\t Sent CLMUL to dut"))
     57 : `logLevel( bitmanip,1,$format ("\t Sent CLMULR to dut"))
     58 : `logLevel( bitmanip,1,$format ("\t Sent CLMULH to dut"))
     59 : `logLevel( bitmanip,1,$format ("\t Sent SH1ADDUW to dut"))
     60 : `logLevel( bitmanip,1,$format ("\t Sent SH2ADDUW to dut"))

     61 : `logLevel( bitmanip,1,$format ("\t Sent SH3ADDUW to dut"))
     62 : `logLevel( bitmanip,1,$format ("\t Sent CMIX to dut"))
     63 : `logLevel( bitmanip,1,$format ("\t Sent CMOV to dut"))
     64 : `logLevel( bitmanip,1,$format ("\t Sent FSL to dut"))
     65 : `logLevel( bitmanip,1,$format ("\t Sent FSR to dut"))
     66 : `logLevel( bitmanip,1,$format ("\t Sent CLMULW to dut"))
     67 : `logLevel( bitmanip,1,$format ("\t Sent CLMULRW to dut"))
     68 : `logLevel( bitmanip,1,$format ("\t Sent CLMULHW to dut"))
     69 : `logLevel( bitmanip,1,$format ("\t Sent BEXT to dut"))
     70 : `logLevel( bitmanip,1,$format ("\t Sent BDEP to dut"))

     71 : `logLevel( bitmanip,1,$format ("\t Sent BEXTW to dut"))
     72 : `logLevel( bitmanip,1,$format ("\t Sent BDEPW to dut"))
     73 : `logLevel( bitmanip,1,$format ("\t Sent CRC32.B to dut"))
     74 : `logLevel( bitmanip,1,$format ("\t Sent CRC32.H to dut"))
     75 : `logLevel( bitmanip,1,$format ("\t Sent CRC32.W to dut"))
     76 : `logLevel( bitmanip,1,$format ("\t Sent CRC32.D to dut"))
     77 : `logLevel( bitmanip,1,$format ("\t Sent CRC32C.B to dut"))
     78 : `logLevel( bitmanip,1,$format ("\t Sent CRC32C.H to dut"))
     79 : `logLevel( bitmanip,1,$format ("\t Sent CRC32C.W to dut"))
     80 : `logLevel( bitmanip,1,$format ("\t Sent CRC32C.D to dut"))

     81 : `logLevel( bitmanip,1,$format ("\t Sent PACKH to dut"))
     82 : `logLevel( bitmanip,1,$format ("\t Sent SHFL to dut"))
     83 : `logLevel( bitmanip,1,$format ("\t Sent UNSHFL to dut"))
     84 : `logLevel( bitmanip,1,$format ("\t Sent SHFLI to dut"))
     85 : `logLevel( bitmanip,1,$format ("\t Sent UNSHFLI to dut"))
     86 : `logLevel( bitmanip,1,$format ("\t Sent SHFLW to dut"))
     87 : `logLevel( bitmanip,1,$format ("\t Sent UNSHFLW to dut"))
     88 : `logLevel( bitmanip,1,$format ("\t Sent BFP to dut"))
     89 : `logLevel( bitmanip,1,$format ("\t Sent BFPW to dut"))
     90 : `logLevel( bitmanip,1,$format ("\t Sent PACKLW to dut"))

     91 : `logLevel( bitmanip,1,$format ("\t Sent PACKUW to dut"))
     92 : `logLevel( bitmanip,1,$format ("\t Sent FSLW to dut"))
     93 : `logLevel( bitmanip,1,$format ("\t Sent FSRW to dut"))
     94 : `logLevel( bitmanip,1,$format ("\t Sent CLZW to dut"))
     95 : `logLevel( bitmanip,1,$format ("\t Sent CTZW to dut"))
     96 : `logLevel( bitmanip,1,$format ("\t Sent PCNTW to dut"))
     97 : `logLevel( bitmanip,1,$format ("\t Sent SEXT.B to dut"))
     98 : `logLevel( bitmanip,1,$format ("\t Sent SEXT.H to dut"))
     99 : `logLevel( bitmanip,1,$format ("\t Sent BMATFLIP to dut"))
     100 : `logLevel( bitmanip,1,$format ("\t Sent BMATXOR to dut"))

     101 : `logLevel( bitmanip,1,$format ("\t Sent BMATOR to dut"))
     102 : `logLevel( bitmanip,1,$format ("\t Sent FSRIW to dut"))
     103 : `logLevel( bitmanip,1,$format ("\t Sent FSRI to dut"))
     104 : `logLevel( bitmanip,1,$format ("\t Sent ADDIWU to dut"))
     105 : `logLevel( bitmanip,1,$format ("\t Sent SLLIU.W to dut"))
     default:`logLevel( bitmanip,1,$format ("\t Sent ILLEGAL INSTR to dut"))
endcase
endaction;
endfunction

(* synthesize *)
module mkTb_bitmanip (Empty);
  Reg#(Bit#(32)) rg_count <- mkReg(0);
   Ifc_bitmanip bitmanip <- mkbitmanip;
   Command cmnd[`TESTCASES] ;

   cmnd[0].instr  = (`ANDN_TEST_CASE);
   cmnd[0].src1   = ('d1);
   cmnd[0].src2   = ('d0);

   cmnd[1].instr  = (`ORN_TEST_CASE);
   cmnd[1].src1   = ('h0);
   cmnd[1].src2   = ('h1);

   cmnd[2].instr  = (`XORN_TEST_CASE);
   cmnd[2].src1   = ('h0);
   cmnd[2].src2   = ('h0);

   cmnd[3].instr  = (`SLO_TEST_CASE);
   cmnd[3].src1   = ('hDEADBEEF);
   cmnd[3].src2   = ('h8);

   cmnd[4].instr  = (`SRO_TEST_CASE);
   cmnd[4].src1   = ('hDEADBEEF);
   cmnd[4].src2   = ('h8);

   cmnd[5].instr  = (`ROL_TEST_CASE);
   cmnd[5].src1   = ('hDEADBEEF);
   cmnd[5].src2   = ('h4);

   cmnd[6].instr  = (`ROR_TEST_CASE);
   cmnd[6].src1   = ('hDEADBEEF);
   cmnd[6].src2   = ('h4);

   cmnd[7].instr  = (`SBCLR_TEST_CASE);
   cmnd[7].src1   = ('hDEADBEEF);
   cmnd[7].src2   = ('h1);

   cmnd[8].instr  = (`SBEXT_TEST_CASE);
   cmnd[8].src1   = ('hDEADBEEF);
   cmnd[8].src2   = ('hE);

   cmnd[9].instr  = (`SBSET_TEST_CASE);
   cmnd[9].src1   = ('ha1cca08);
   cmnd[9].src2   = ('h155171d);
   cmnd[9].src3   = ('haeb38ba);

   cmnd[10].instr = (`SBINV_TEST_CASE);
   cmnd[10].src1  = ('hb3d3eff);
   cmnd[10].src2  = ('h8db18b7);
   cmnd[10].src3  = ('h5735e15);


   cmnd[11].instr = (`PACKL_TEST_CASE);
   cmnd[11].src1  = ('hDEADBEEF);
   cmnd[11].src2  = ('hCAFEBBBB);

   cmnd[12].instr = (`PACKU_TEST_CASE);
   cmnd[12].src1  = ('hDEADBEEF);
   cmnd[12].src2  = ('hCAFEBBBB);

   cmnd[13].instr = (`SLOI_TEST_CASE);
   cmnd[13].src1  = ('hDEADBEEF);


   cmnd[14].instr = (`SROI_TEST_CASE);
   cmnd[14].src1  = ('hDEADBEEF);


   cmnd[15].instr = (`RORI_TEST_CASE);
   cmnd[15].src1  = ('hDEADBEEF);

   cmnd[16].instr = (`SH1ADD_TEST_CASE);
   cmnd[16].src1  = ('h00000001);
   cmnd[16].src2  = ('h00000001);

   cmnd[17].instr = (`SH2ADD_TEST_CASE);
   cmnd[17].src1  = ('h00000001);
   cmnd[17].src2  = ('h00000001);

   cmnd[18].instr = (`SH3ADD_TEST_CASE);
   cmnd[18].src1  = ('h00000001);
   cmnd[18].src2  = ('h00000001);

   cmnd[19].instr = (`MIN_TEST_CASE);
   cmnd[19].src1  = ('d-4);
   cmnd[19].src2  = ('d4);

   cmnd[20].instr = (`MAX_TEST_CASE);
   cmnd[20].src1  = ('d-4);
   cmnd[20].src2  = ('d5);

   cmnd[21].instr = (`MINU_TEST_CASE);
   cmnd[21].src1  = ('d-4);
   cmnd[21].src2  = ('d8);

   cmnd[22].instr = (`MAXU_TEST_CASE);
   cmnd[22].src1  = ('d-4);
   cmnd[22].src2  = ('d6);

   cmnd[23].instr = (`ADDWU_TEST_CASE);
   cmnd[23].src1  = (`ifdef RV64 'hffff0000ffff1111 `else 'hff00ff11 `endif );
   cmnd[23].src2  = (`ifdef RV64 'h0000000000000001 `else 'h00000001 `endif );

   cmnd[24].instr = (`SUBWU_TEST_CASE);
   cmnd[24].src1  = (`ifdef RV64 'hffff0000ffff1111 `else 'hff00ff11 `endif );
   cmnd[24].src2  = (`ifdef RV64 'h0000000000000001 `else 'h00000001 `endif );

   cmnd[25].instr = (`ADDU_W_TEST_CASE);
   cmnd[25].src1  = (`ifdef RV64 'hffff0000ffff1111 `else 'hff00ff11 `endif );
   cmnd[25].src2  = (`ifdef RV64 'hffff000000000001 `else 'h00000001 `endif );

   cmnd[26].instr = (`SUBU_W_TEST_CASE);
   cmnd[26].src1  = (`ifdef RV64 'hffff0000ffff1111 `else 'hff00ff11 `endif );
   cmnd[26].src2  = (`ifdef RV64 'hffff000000000001 `else 'h00000001 `endif );

   cmnd[27].instr = (`SLOW_TEST_CASE);
   cmnd[27].src1  = (`ifdef RV64 'hffffffff12341234 `else 'hffff1234 `endif );
   cmnd[27].src2  = (`ifdef RV64 'h0000000000000004 `else 'h00000004 `endif );

   cmnd[28].instr = (`SROW_TEST_CASE);
   cmnd[28].src1  = (`ifdef RV64 'hffffffff12341234 `else 'hffff1234 `endif );
   cmnd[28].src2  = (`ifdef RV64 'h0000000000000004 `else 'h00000004 `endif );

   cmnd[29].instr = (`RORW_TEST_CASE);
   cmnd[29].src1  = (`ifdef RV64 'hffffffff12341234 `else 'hffff1234 `endif );
   cmnd[29].src2  = (`ifdef RV64 'h0000000000000004 `else 'h00000004 `endif );

   cmnd[30].instr = (`ROLW_TEST_CASE);
   cmnd[30].src1  = (`ifdef RV64 'hffffffff12341234 `else 'hffff1234 `endif );
   cmnd[30].src2  = (`ifdef RV64 'h0000000000000004 `else 'h00000004 `endif );

   cmnd[31].instr = (`GREV_TEST_CASE);
   cmnd[31].src1  = ('h12341234);
   cmnd[31].src2  = ('h00000004);

   cmnd[32].instr = (`SBCLRI_TEST_CASE);
   cmnd[32].src1  = ('hDEADBEEF);

   cmnd[33].instr = (`SBINVI_TEST_CASE);
   cmnd[33].src1  = ('hDEADBEEF);


   cmnd[34].instr = (`SBEXTI_TEST_CASE);
   cmnd[34].src1  = ('hDEADBEEF);


   cmnd[35].instr = ('h2cfa1393);
   cmnd[35].src1  = ('h1e5a1b3d);


   cmnd[36].instr = (`GREVI_TEST_CASE);
   cmnd[36].src1  = ('h12341234);



   cmnd[37].instr  = (`GORC_TEST_CASE);
   cmnd[37].src1   = ('h12341234);
   cmnd[37].src2   = ('h00000004);

   cmnd[38].instr  = (`GORCI_TEST_CASE);
   cmnd[38].src1   = ('h12341234);



   cmnd[39].instr  = (`GORCIW_TEST_CASE);
   cmnd[39].src1   = (`ifdef RV64 'hffffffff12341234 `else 'hffff1234 `endif );


   cmnd[40].instr  = (`GREVW_TEST_CASE);
   cmnd[40].src1   = (`ifdef RV64 'hffffffff12341234 `else 'hffff1234 `endif );
   cmnd[40].src2   = (`ifdef RV64 'hffffffff00000004 `else 'hffff0004 `endif );

   cmnd[41].instr  = (`GREVIW_TEST_CASE);
   cmnd[41].src1   = (`ifdef RV64 'hffffffff12341234 `else 'hffff1234 `endif );

   cmnd[42].instr  = (`GORCW_TEST_CASE);
   cmnd[42].src1   = (`ifdef RV64 'hffffffff12341234 `else 'hffff1234 `endif );
   cmnd[42].src2   = (`ifdef RV64 'hffffffff00000004 `else 'hffff0004 `endif );

   cmnd[43].instr  = (`SBCLRW_TEST_CASE);
   cmnd[43].src1   = (`ifdef RV64 'hffffffffDEADBEEF `else 'hffffDEAD `endif );
   cmnd[43].src2   = (`ifdef RV64 'hffffffff00000001 `else 'hffff0001 `endif );

   cmnd[44].instr  = (`SBEXTW_TEST_CASE);
   cmnd[44].src1   = (`ifdef RV64 'hffffffffDEADBEEF `else 'hffffDEAD `endif );
   cmnd[44].src2   = (`ifdef RV64 'hffffffff0000000E `else 'hffff000E `endif );

   cmnd[45].instr  = (`SBSETW_TEST_CASE);
   cmnd[45].src1   = (`ifdef RV64 'hffffffffDEADBEEF `else 'hffffDEAD `endif );
   cmnd[45].src2   = (`ifdef RV64 'hffffffff00000004 `else 'hffff0004 `endif );

   cmnd[46].instr = (`SBINVW_TEST_CASE);
   cmnd[46].src1  = (`ifdef RV64 'hffffffffDEADBEEF `else 'hffffDEAD `endif );
   cmnd[46].src2  = (`ifdef RV64 'hffffffff00000002 `else 'hffff0002 `endif );

   cmnd[47].instr = (`SBCLRIW_TEST_CASE);
   cmnd[47].src1  = (`ifdef RV64 'hFFFFFFFFDEADBEEF `else 'hffffDEAD `endif );


   cmnd[48].instr = (`SBSETIW_TEST_CASE);
   cmnd[48].src1  = (`ifdef RV64 'hffffffffDEADBEEF `else 'hffffDEAD `endif );


   cmnd[49].instr = (`SLOIW_TEST_CASE);
   cmnd[49].src1  = (`ifdef RV64 'hffffffffDEADBEEF `else 'hffffDEAD `endif );


   cmnd[50].instr = (`SROIW_TEST_CASE);
   cmnd[50].src1  = ('hDEADBEEF);


   cmnd[51].instr = (`RORIW_TEST_CASE);
   cmnd[51].src1  = (`ifdef RV64 'hffffffffDEADBEEF `else 'hffffDEAD `endif );


   cmnd[52].instr = (`SBINVIW_TEST_CASE);
   cmnd[52].src1  = (`ifdef RV64 'hffffffffDEADBEEF `else 'hffffDEAD `endif );

   cmnd[53].instr  = (`CLZ_TEST_CASE);
   cmnd[53].src1   = ('hade0cb85);

   cmnd[54].instr  = (`CTZ_TEST_CASE);
   cmnd[54].src1   = ('h12341234);

   cmnd[55].instr  = (`PCNT_TEST_CASE);
   cmnd[55].src1   = ('h12341234);

   cmnd[56].instr  = (`CLMUL_TEST_CASE);
   cmnd[56].src1   = ('h00000002);
   cmnd[56].src2   = ('h00000004);

   cmnd[57].instr  = (`CLMULR_TEST_CASE);
   cmnd[57].src1   = ('h00000002);
   cmnd[57].src2   = ('h00000004);

   cmnd[58].instr  = (`CLMULH_TEST_CASE);
   cmnd[58].src1   = ('h00000002);
   cmnd[58].src2   = ('h00000004);

   cmnd[59].instr  = (`SH1ADDUW_TEST_CASE);
   cmnd[59].src1   = (`ifdef RV64 'hffffffff00000001 `else 'hffff0001 `endif );
   cmnd[59].src2  = ('h00000001);

   cmnd[60].instr  = (`SH2ADDUW_TEST_CASE);
   cmnd[60].src1   = (`ifdef RV64 'hffffffff00000001 `else 'hffff0001 `endif );
   cmnd[60].src2   = ('h00000001);

   cmnd[61].instr  = (`SH3ADDUW_TEST_CASE);
   cmnd[61].src1   = (`ifdef RV64 'hffffffff00000001 `else 'hffff0001 `endif );
   cmnd[61].src2   = ('h00000001);

   cmnd[62].instr  = (`CMIX_TEST_CASE);
   cmnd[62].src1   = ('h00000002);
   cmnd[62].src2   = ('h00000001);
   cmnd[62].src3   = ('h00000003);

   cmnd[63].instr  = (`CMOV_TEST_CASE);
   cmnd[63].src1   = ('h00000002);
   cmnd[63].src2   = ('h00000001);
   cmnd[63].src3   = ('h00000003);

   cmnd[64].instr  = (`FSL_TEST_CASE);
   cmnd[64].src1   = ('h00000001);
   cmnd[64].src2   = ('h00000001);
   cmnd[64].src3   = ('h00000001);

   cmnd[65].instr  = (`FSR_TEST_CASE);
   cmnd[65].src1   = ('h00000002);
   cmnd[65].src2   = ('h00000000);
   cmnd[65].src3   = ('h00000000);

   cmnd[66].instr  = (`CLMULW_TEST_CASE);
   cmnd[66].src1   = ('h00000002);
   cmnd[66].src2   = ('h00000004);

   cmnd[67].instr  = (`CLMULRW_TEST_CASE);
   cmnd[67].src1   = ('h00000002);
   cmnd[67].src2   = ('h00000004);

   cmnd[68].instr  = (`CLMULHW_TEST_CASE);
   cmnd[68].src1   = ('h00000002);
   cmnd[68].src2   = ('h00000004);

   cmnd[69].instr  = (`BEXT_TEST_CASE);
   cmnd[69].src1   = ('h000000AE);
   cmnd[69].src2   = ('h0000000A);

   cmnd[70].instr  = (`BDEP_TEST_CASE);
   cmnd[70].src1   = ('h000000FA);
   cmnd[70].src2   = ('h0000000E);

   cmnd[71].instr  = (`BEXTW_TEST_CASE);
   cmnd[71].src1   = ('h000000AE);
   cmnd[71].src2   = ('h0000000A);

   cmnd[72].instr  = (`BDEPW_TEST_CASE);
   cmnd[72].src1   = ('h000000FA);
   cmnd[72].src2   = ('h0000000E);

   cmnd[73].instr  = (`CRC32_B_TEST_CASE);
   cmnd[73].src1   = ('hFA);


   cmnd[74].instr  = (`CRC32_H_TEST_CASE);
   cmnd[74].src1   = ('h00FA);


   cmnd[75].instr  = (`CRC32_W_TEST_CASE);
   cmnd[75].src1   = ('h000000FA);



   cmnd[76].instr  = (`CRC32_D_TEST_CASE);
   cmnd[76].src1   = (`ifdef RV64 'h000000FA000000FA `else 'h00FA00FA `endif );

   cmnd[77].instr  = (`CRC32C_B_TEST_CASE);
   cmnd[77].src1   = ('hFA);


   cmnd[78].instr  = (`CRC32C_H_TEST_CASE);
   cmnd[78].src1   = ('h00FA);


   cmnd[79].instr  = (`CRC32C_W_TEST_CASE);
   cmnd[79].src1   = ('h000000FA);



   cmnd[80].instr  = (`CRC32C_D_TEST_CASE);
   cmnd[80].src1   = (`ifdef RV64 'h000000FA000000FA `else 'h00FA00FA `endif );

   cmnd[81].instr = (`PACKH_TEST_CASE);
   cmnd[81].src1  = ('hDEADBEEF);
   cmnd[81].src2  = ('hCAFEBBBB);

   cmnd[82].instr = (`SHFL_TEST_CASE);
   cmnd[82].src1  = ('h12341234);
   cmnd[82].src2  = ('h00000004);

   cmnd[83].instr = (`UNSHFL_TEST_CASE);
   cmnd[83].src1  = ('h12341234);
   cmnd[83].src2  = ('h00000004);

   cmnd[84].instr = (`SHFLI_TEST_CASE);
   cmnd[84].src1  = ('h12341234);


   cmnd[85].instr = (`UNSHFLI_TEST_CASE);
   cmnd[85].src1  = ('h12341234);


   cmnd[86].instr = (`SHFLW_TEST_CASE);
   cmnd[86].src1  = ('h12341234);
   cmnd[86].src2  = ('h00000004);

   cmnd[87].instr = (`UNSHFLW_TEST_CASE);
   cmnd[87].src1  = ('h12341234);
   cmnd[87].src2  = ('h00000004);

   cmnd[88].instr = (`BFP_TEST_CASE);
   cmnd[88].src1  = ('h12341234);
   cmnd[88].src2  = ('h00000004);

   cmnd[89].instr = (`BFPW_TEST_CASE);
   cmnd[89].src1  = ('h12341234);
   cmnd[89].src2  = ('h00000004);

   cmnd[90].instr = (`PACKLW_TEST_CASE);
   cmnd[90].src1  = ('h12341234);
   cmnd[90].src2  = ('h00000004);

   cmnd[91].instr = (`PACKUW_TEST_CASE);
   cmnd[91].src1  = ('h12341234);
   cmnd[91].src2  = ('h00000004);

   cmnd[92].instr  = (`FSLW_TEST_CASE);
   cmnd[92].src1   = ('h00000001);
   cmnd[92].src2   = ('h00000001);
   cmnd[92].src3   = ('h00000001);

   cmnd[93].instr  = (`FSRW_TEST_CASE);
   cmnd[93].src1   = ('h00000002);
   cmnd[93].src2   = ('h00000001);
   cmnd[93].src3   = ('h00000001);

   cmnd[94].instr  = (`CLZW_TEST_CASE);
   cmnd[94].src1   = ('h02341234);

   cmnd[95].instr  = (`CTZW_TEST_CASE);
   cmnd[95].src1   = ('h12341234);

   cmnd[96].instr  = (`PCNTW_TEST_CASE);
   cmnd[96].src1   = ('h12341234);

   cmnd[97].instr  = (`SEXT_B_TEST_CASE);
   cmnd[97].src1   = ('h02341234);

   cmnd[98].instr  = (`SEXT_H_TEST_CASE);
   cmnd[98].src1   = ('h12341234);


   cmnd[99].instr  = (`BMATFLIP_TEST_CASE);
   cmnd[99].src1   = (`ifdef RV64 'h1234123412341234 `else 'h12341234 `endif );

   cmnd[100].instr  = (`BMATXOR_TEST_CASE);
   cmnd[100].src1   = (`ifdef RV64 'h1234123412341234 `else 'h12341234 `endif );
   cmnd[100].src2   = (`ifdef RV64 'h1234123412341234 `else 'h12341234 `endif );

   cmnd[101].instr  = (`BMATOR_TEST_CASE);
   cmnd[101].src1   = (`ifdef RV64 'h1234123412341234 `else 'h12341234 `endif );
   cmnd[101].src2   = (`ifdef RV64 'h1234123412341234 `else 'h12341234 `endif );

   cmnd[102].instr  = (`FSRIW_TEST_CASE);
   cmnd[102].src1   = ('h00000001);
   cmnd[102].src3   = ('h00000001);

   cmnd[103].instr  = ('h0f5adc13);
   cmnd[103].src1   = ('h3a5ba836);
   cmnd[103].src3   = ('h09fbbbc4);

   cmnd[104].instr = (`ADDIWU_TEST_CASE);
   cmnd[104].src1  = (`ifdef RV64 'hffff0000ffff1111 `else 'hff00ff11 `endif );


   cmnd[105].instr = (`SLLIU_W_TEST_CASE);
   cmnd[105].src1  = (`ifdef RV64 'hffff0000ffff1111 `else 'hff00ff11 `endif );

   cmnd[106].instr = (`ILLEGAL_INSTR_TEST_CASE);

   rule rl_start(rg_count < `TESTCASES);
    passString(rg_count);
    let result <- bitmanip.mav_putvalue(cmnd[rg_count].instr, cmnd[rg_count].src1,
                                      cmnd[rg_count].src2, cmnd[rg_count].src3);
`ifdef RV64     if(pack(tpl_1(result))==fn_checktest_64(rg_count))
                `logLevel( bitmanip,1,$format("PASSED"))
                 else `logLevel( bitmanip,1,$format("failed")) `endif
`ifdef RV32     if(pack(tpl_1(result))==fn_checktest_32(rg_count) && tpl_2(result))`logLevel( bitmanip,1,$format("PASSED")) else if(pack(tpl_1(result))!=fn_checktest_32(rg_count) && tpl_2(result))`logLevel( bitmanip,1,$format("FAILED"))  else `logLevel( bitmanip,1,$format("INVALID")) `endif
     `logLevel( bitmanip,1,$format(" \t inst= %0x\tr rs1 = %0x\trs2 = %0x \t rs3 = %0x", cmnd[rg_count].instr,cmnd[rg_count].src1, cmnd[rg_count].src2, cmnd[rg_count].src3))
     `logLevel( bitmanip,1,$format ("\t Valid Data = %0b \t rd = %0x\n",tpl_2(result),pack(tpl_1(result))))
    rg_count <= rg_count + 1;
    if(rg_count == (`TESTCASES -1))
      $finish;
   endrule
endmodule
endpackage
