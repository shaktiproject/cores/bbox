 /*doc:func: shift rs1 left by 1 bit, then add the result to rs2  ,Bit#(1) w_inst */
 function Bit#(XLEN) fn_shnadd(Bit#(XLEN) src1,Bit#(XLEN) src2, Bit#(2) shamt) provisos(Arith#(Bit#(XLEN))) ;
    return ((src1 << shamt) + src2);
 endfunction
