SOC
=====================================

Contents:

.. toctree::
   :maxdepth: 2
   :glob:

   introduction
   interface

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
